<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pause" xml:lang="ca">
      
  <info>
    <title type="sort">4</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="newgame"/>
    <desc>Com fer pausa o finalitzar una partida.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Reconeixement-Compartir Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2009-2018</mal:years>
    </mal:credit>
  </info>

  <title>Fer pausa o finalitzar una partida</title>

  <section id="pause-pause">
    <title>Fer pausa o reprendre una partida</title>
    <p>Per a fer pausa a una partida i així poder continuar-la al mateix punt més endavant, feu una de les accions següents:</p>
    <list>
      <item>
        <p>Seleccioneu <guiseq><gui style="menu">Partida</gui><gui style="menuitem">Fes una pausa a la partida</gui></guiseq>.</p>
      </item>
      <item>
        <p>Feu clic al botó <gui style="button">Pausa</gui> a la barra d'eines.</p>
      </item>
    </list>
    <p>Per tal de continuar el joc després d'haver fet una pausa, feu una de les accions següents:</p>
    <list>
      <item>
        <p>Seleccioneu <guiseq><gui style="menu">Partida</gui><gui style="menuitem">Fes una pausa a la partida</gui></guiseq>.</p>
      </item>
      <item>
        <p>Feu clic al botó <gui style="button">Reprèn</gui> a la barra d'eines.</p>
      </item>
    </list>
  </section>
  
  <section id="pause-stop">
    <title>Finalitzar una partida</title>
    <p>Per a finalitzar la partida, feu una de les accions següents:</p>
    <list>
      <item>
        <p>Seleccioneu <guiseq><gui style="menu">Partida</gui><gui style="menuitem">Finalitza la partida</gui></guiseq>.</p>
      </item>
      <item>
        <p>Feu clic al botó <gui style="button">Finalitza</gui> a la barra d'eines.</p>
      </item>
    </list>
  </section>
</page>
