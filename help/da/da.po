# Danish translation for gbrainy.
# Copyright (C) 2018 gbrainy's COPYRIGHT HOLDER
# This file is distributed under the same license as the gbrainy package.
# Joe Hansen (joedalton2@yahoo.dk), 2009, 2010, 2011, 2015, 2018.
#
# spin box -> rulleboks
#
msgid ""
msgstr ""
"Project-Id-Version: gbrainy master\n"
"POT-Creation-Date: 2018-03-11 09:21+0000\n"
"PO-Revision-Date: 2018-04-02 23:07+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Dansk-gruppen <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Joe Hansen, 2009-2018\n"
"\n"
"Dansk-gruppen <dansk@dansk-gruppen.dk>\n"
"Mere info: http://www.dansk-gruppen.dk"

#. (itstool) path: info/desc
#: C/customgame.page:9
msgid "Start a custom games selection."
msgstr "Begynd med en tilpasset spilmarkering."

#. (itstool) path: credit/name
#: C/customgame.page:12 C/pause.page:12 C/tips.page:12 C/difficulty.page:12
#: C/history.page:9 C/multiplayer.page:11 C/score.page:11 C/gameplay.page:12
#: C/index.page:15 C/newgame.page:14 C/times.page:11 C/export.page:10
msgid "Milo Casagrande"
msgstr "Milo Casagrande"

#. (itstool) path: license/p
#: C/customgame.page:16 C/gametypes.page:13 C/pause.page:16 C/tips.page:16
#: C/difficulty.page:16 C/history.page:13 C/multiplayer.page:15 C/score.page:15
#: C/gameplay.page:16 C/index.page:19 C/newgame.page:18 C/times.page:15
#: C/export.page:14
msgid "Creative Commons Share Alike 3.0"
msgstr "Creative Commons Share Alike 3.0"

#. (itstool) path: page/title
#: C/customgame.page:20
msgid "Custom games selection"
msgstr "Tilpasset spilmarkering"

#. (itstool) path: page/p
#: C/customgame.page:21
msgid ""
"It is possible to play a custom selection of all the games in <app>gbrainy</"
"app>."
msgstr ""
"Det er muligt at spille med en tilpasset spilmarkering blandt alle spil i "
"<app>gbrainy</app>."

#. (itstool) path: page/p
#: C/customgame.page:24
msgid "To do so, proceed as follows:"
msgstr "Det gøres på den følgende måde:"

#. (itstool) path: item/p
#: C/customgame.page:29
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New "
"Game</gui> <gui style=\"menuitem\">Custom Game Selection</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Spil</gui><gui style=\"menuitem\">Nyt spil</"
"gui> <gui style=\"menuitem\">Tilpasset spiludvælgelse</gui></guiseq>."

#. (itstool) path: item/p
#: C/customgame.page:35
msgid ""
"In the <gui>Custom Game</gui> dialog, select the games that you would like "
"to play by clicking on the check box next to the name of the game."
msgstr ""
"I dialogen <gui>Tilpasset spil</gui> vælges spillet som du ønsker at spille "
"ved at klikke på afkrydsningsboksen ved siden af navnet på spillet."

#. (itstool) path: item/p
#: C/customgame.page:41
msgid ""
"Once you are done, click on <gui style=\"button\">Start</gui> to start "
"playing your custom games selection."
msgstr ""
"Når du er færdig, klik på <gui style=\"button\">Start</gui> for at starte et "
"spil med dine tilpasninger."

#. (itstool) path: info/title
#: C/gametypes.page:6
msgctxt "sort"
msgid "3"
msgstr "3"

#. (itstool) path: info/desc
#: C/gametypes.page:7
msgid "What types of game you can play."
msgstr "De typer af spil du kan spille."

#. (itstool) path: page/title
#: C/gametypes.page:16
msgid "Game types"
msgstr "Spiltyper"

#. (itstool) path: page/p
#: C/gametypes.page:18
msgid "<app>gbrainy</app> provides the following types of games:"
msgstr "<app>gbrainy</app> har de følgende spiltyper:"

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:27 C/score.page:39
msgid "Logic puzzles"
msgstr "Logiske puslespil"

#. (itstool) path: item/p
#: C/gametypes.page:28
msgid ""
"Games designed to challenge your reasoning and thinking skills. These games "
"are based on sequences of elements, visual and spatial reasoning or "
"relationships between elements."
msgstr ""
"Spil der er designet til at udfordre dine logiske sans og tænkemåde. Disse "
"spil er baseret på sekvenser af elementer, visuel og rumlig tankegang eller "
"forhold mellem elementer."

#. (itstool) path: item/title
#: C/gametypes.page:48
msgid "Mental calculations"
msgstr "Hovedregning"

#. (itstool) path: item/p
#: C/gametypes.page:49
msgid ""
"Games based on arithmetical operations designed to improve your mental "
"calculation skills. Games that require the player to use multiplication, "
"division, addition and subtraction combined in different ways."
msgstr ""
"Spil baseret på matematik, der skal vise dine evner indenfor hovedregning. "
"Det er spil, som øver brugeren i gange, division, plus og minus kombineret "
"på forskellig vis."

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:69 C/score.page:51
msgid "Memory trainers"
msgstr "Hukommelsesspil"

#. (itstool) path: item/p
#: C/gametypes.page:70
msgid ""
"Games designed to challenge your short-term memory. These games show "
"collections of objects and ask the player to recall them, sometimes "
"establishing relationships between figures, words, numbers or colors."
msgstr ""
"Spil der er designet til at udfordre din korttidshukommelse. Disse spil "
"viser samlinger af objekter og beder spilleren om at huske dem. Undertiden "
"indgår sammenhænge mellem figurerne, ordene, tallene eller farverne."

#. (itstool) path: item/title
#. (itstool) path: td/p
#: C/gametypes.page:90 C/score.page:57
msgid "Verbal analogies"
msgstr "Sproglige analogier"

#. (itstool) path: item/p
#: C/gametypes.page:91
msgid ""
"Games that challenge your verbal aptitude. These games ask the player to "
"identify cause and effect, use synonyms or antonyms, and use their "
"vocabulary."
msgstr ""
"Spil som udfordrer dine sproglige talenter. Det er spil, som beder spilleren "
"om at identificere årsag og effekt, bruge synonymer eller ord med modsat "
"betydning, og bruge sit ordforråd."

#. (itstool) path: info/desc
#: C/license.page:8
msgid "Legal information."
msgstr "Juridisk information."

#. (itstool) path: page/title
#: C/license.page:11
msgid "License"
msgstr "Licens"

#. (itstool) path: page/p
#: C/license.page:12
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 "
"Unported license."
msgstr ""
"Dette arbejde er udgivet under licensen CreativeCommons Attribution-Share "
"Alike 3.0 Unported."

#. (itstool) path: page/p
#: C/license.page:18
msgid "You are free:"
msgstr "Du kan frit:"

#. (itstool) path: item/title
#: C/license.page:23
msgid "To share"
msgstr "Dele"

#. (itstool) path: item/p
#: C/license.page:24
msgid "To copy, distribute and transmit the work."
msgstr "Kopiere, distribuere og overføre arbejdet."

#. (itstool) path: item/title
#: C/license.page:27
msgid "To remix"
msgstr "Blande"

#. (itstool) path: item/p
#: C/license.page:28
msgid "To adapt the work."
msgstr "Tilpasse arbejdet."

#. (itstool) path: page/p
#: C/license.page:31
msgid "Under the following conditions:"
msgstr "Under de følgende betingelser:"

#. (itstool) path: item/title
#: C/license.page:36
msgid "Attribution"
msgstr "Kreditering"

#. (itstool) path: item/p
#: C/license.page:37
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use "
"of the work)."
msgstr ""
"Du skal kreditere arbejdet på den måde som er angivet af forfatteren eller "
"licensindehaveren (men ikke på en måde som indikerer at de støtter dig eller "
"din brug af deres arbejde)."

#. (itstool) path: item/title
#: C/license.page:44
msgid "Share Alike"
msgstr "Share Alike"

#. (itstool) path: item/p
#: C/license.page:45
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr ""
"Hvis du ændrer, omformer, eller bygger videre på dette arbejde, må du "
"distribuere det færdige arbejde under den samme, lignende eller kompatibel "
"licens."

#. (itstool) path: page/p
#: C/license.page:51
msgid ""
"For the full text of the license, see the <link href=\"https://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</"
"link>, or read the full <link href=\"https://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."
msgstr ""
"For den fulde licenstekst, se <link href=\"https://creativecommons.org/"
"licenses/by-sa/3.0/legalcode\">CreativeCommons hjemmeside</link>, eller læs "
"de fulde <link href=\"http://creativecommons.org/licenses/by-sa/3.0/"
"\">Commons Deed</link>."

#. (itstool) path: info/title
#: C/pause.page:6
msgctxt "sort"
msgid "4"
msgstr "4"

#. (itstool) path: info/desc
#: C/pause.page:9
msgid "How to pause or end a game."
msgstr "Hvordan du afslutter og sætter et spil på pause."

#. (itstool) path: page/title
#: C/pause.page:20
msgid "Pause/End a game"
msgstr "Pause/afslut et spil"

#. (itstool) path: section/title
#: C/pause.page:23
msgid "Pause and resume a game"
msgstr "Pause og genoptag et spil"

#. (itstool) path: section/p
#: C/pause.page:24
msgid ""
"To pause a game so that you can resume it at the same point at a later time, "
"perform one of the following:"
msgstr ""
"For at sætte et spil på pause, så du kan genoptage det samme sted på et "
"senere tidspunkt, udføres en af følgende muligheder:"

#. (itstool) path: item/p
#: C/pause.page:30 C/pause.page:45
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">Pause "
"Game</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Spil</gui><gui style=\"menuitem\">Pause</"
"gui></guiseq>."

#. (itstool) path: item/p
#: C/pause.page:35
msgid "Click on the <gui style=\"button\">Pause</gui> button in the toolbar."
msgstr "Klik på knappen <gui style=\"button\">Pause</gui> i værktøjsbjælken."

#. (itstool) path: section/p
#: C/pause.page:40
msgid ""
"In order to resume the game after you paused it, perform one of the "
"following:"
msgstr ""
"For at genoptage spillet efter en pause, udføres en af følgende muligheder:"

#. (itstool) path: item/p
#: C/pause.page:50
msgid "Click on the <gui style=\"button\">Resume</gui> button in the toolbar."
msgstr ""
"Klik på knappen <gui style=\"button\">Genoptag</gui> i værktøjsbjælken."

#. (itstool) path: section/title
#: C/pause.page:58
msgid "End a game"
msgstr "Afslut et spil"

#. (itstool) path: section/p
#: C/pause.page:59
msgid "To end a game, perform one of the following:"
msgstr "For at afslutte et spil, vælges en af følgende muligheder:"

#. (itstool) path: item/p
#: C/pause.page:64
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">End "
"Game</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Spil</gui><gui style=\"menuitem\">Afslut "
"spil</gui></guiseq>."

#. (itstool) path: item/p
#: C/pause.page:69
msgid "Click on the <gui style=\"button\">End</gui> button in the toolbar."
msgstr "Klik på knappen <gui style=\"button\">Afslut</gui> i værktøjsbjælken."

#. (itstool) path: info/desc
#: C/tips.page:8
msgid "Use the tips to solve a puzzle."
msgstr "Brug fif til at løse et puslespil."

#. (itstool) path: page/title
#: C/tips.page:20
msgid "Tips"
msgstr "Fif"

#. (itstool) path: page/p
#: C/tips.page:21
msgid ""
"With some of the games you can play, it is possible to get simple tips that "
"can help you to solve the problem."
msgstr ""
"I nogle af spillene er det muligt at få simple tip, som kan hjælpe dig med "
"at løse problemet."

#. (itstool) path: page/p
#: C/tips.page:25
msgid ""
"When playing a game, click on the <gui style=\"button\">Tip</gui> button at "
"the bottom of the window (next to the <gui style=\"input\">Answer</gui> text "
"entry)."
msgstr ""
"Når du spiller så klik på knappen <gui style=\"button\">Tip</gui> nederst i "
"vinduet (ved siden af tekstpunktet <gui style=\"input\">Svar</gui>)."

#. (itstool) path: note/p
#: C/tips.page:29
msgid "This feature is not available for some of the games."
msgstr "Denne funktion er ikke tilgængelig for nogle af spillene."

#. (itstool) path: info/title
#: C/difficulty.page:6
msgctxt "sort"
msgid "2"
msgstr "2"

#. (itstool) path: info/desc
#: C/difficulty.page:8
msgid "Change the difficulty level of the games."
msgstr "Ændr sværhedsgrad på spillet."

#. (itstool) path: page/title
#: C/difficulty.page:20
msgid "Difficulty levels"
msgstr "Sværhedsgrader"

#. (itstool) path: page/p
#: C/difficulty.page:21
msgid "To change the difficulty level of the game, proceed as follows:"
msgstr "For at ændre sværhedsgraden på et spil, udfør følgende:"

#. (itstool) path: item/p
#: C/difficulty.page:26 C/times.page:26
msgid ""
"Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem"
"\">Preferences</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Opsætning</gui><gui style=\"menuitem"
"\">Indstillinger</gui></guiseq>."

#. (itstool) path: item/p
#: C/difficulty.page:31
msgid "In the <gui>Difficulty Level</gui> section, select the desired level."
msgstr "I afsnittet <gui>Sværhedsgrad</gui> vælges det ønskede niveau."

#. (itstool) path: item/p
#: C/difficulty.page:34
msgid ""
"You can choose from three different levels: <gui style=\"radiobutton\">Easy</"
"gui>, <gui style=\"radiobutton\">Medium</gui>, and <gui style=\"radiobutton"
"\">Master</gui>. The default level is <gui style=\"radiobutton\">Medium</"
"gui>."
msgstr ""
"Du kan vælge fra tre forskellige niveauer: <gui style=\"radiobutton\">Let</"
"gui>, <gui style=\"radiobutton\">Mellem</gui>, and <gui style=\"radiobutton"
"\">Øvet</gui>. Standardniveauet er <gui style=\"radiobutton\">Mellem</gui>."

#. (itstool) path: info/desc
#: C/history.page:6
msgid "Access your personal game history."
msgstr "Tilgå din personlige spilhistorik."

#. (itstool) path: page/title
#: C/history.page:17
msgid "Personal game history"
msgstr "Personlig historik"

#. (itstool) path: page/p
#: C/history.page:19
msgid ""
"The <gui>Player's Game History</gui> dialog shows your performance in every "
"game type during the last few game sessions."
msgstr ""
"Dialogen <gui>Spillerens spilhistorik</gui> viser dine fremskridt for hver "
"type af spil igennem de sidst optagede spilsessioner."

#. (itstool) path: section/title
#: C/history.page:25
msgid "View the games history"
msgstr "Vis spilhistorikken"

#. (itstool) path: section/p
#: C/history.page:26
msgid ""
"To access your personal game history, choose <guiseq><gui style=\"menu"
"\">View</gui> <gui style=\"menuitem\">Player's Game History</gui></guiseq>."
msgstr ""
"For at tilgå din personlige spilhistorik, vælg <guiseq><gui style=\"menu"
"\">Vis</gui> <gui style=\"menuitem\">Spillerens spilhistorik</gui></guiseq>."

#. (itstool) path: section/title
#: C/history.page:33
msgid "Select which results to show"
msgstr "Vælg hvilke resultater der skal vises"

#. (itstool) path: section/p
#: C/history.page:34
msgid ""
"To select the results that will be shown in the graphic, use the different "
"check boxes located under the graph."
msgstr ""
"For at vælge resultaterne, der skal vises i grafikken, brug de forskellige "
"afkrydningsbokse placeret under grafen."

#. (itstool) path: section/title
#: C/history.page:41
msgid "Change the number of saved games"
msgstr "Ændre antallet af gemte spil"

#. (itstool) path: section/p
#: C/history.page:42
msgid ""
"<app>gbrainy</app> saves the player's scores so it can track how they evolve."
msgstr ""
"<app>gbrainy</app> gemmer spillerens point så spillet kan følge hvordan "
"denne udvikler sig."

#. (itstool) path: section/p
#: C/history.page:45
msgid ""
"In order to change the number of sessions recorded or how many games will be "
"stored in the history, proceed as follows:"
msgstr ""
"For at ændre antallet af sessioner optaget eller hvor mange spil der vil "
"blive gemt i historikken, fortsæt som angivet i det følgende:"

#. (itstool) path: item/p
#: C/history.page:51
msgid ""
"Choose <guiseq><gui style=\"menu\">Settings</gui><gui style=\"menuitem\"> "
"Preferences</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Opsætning</gui><gui style=\"menuitem\"> "
"Indstillinger</gui></guiseq>."

#. (itstool) path: item/p
#: C/history.page:57
msgid "In the <gui>Player's Game History</gui> section:"
msgstr "Du kan tilgå afsnittet <gui>Spillers historik</gui> fra programmenuen:"

#. (itstool) path: item/p
#: C/history.page:62
msgid ""
"Use the first spin box to change how many game sessions need to be recorded "
"before you can start to see the results in the history graphic."
msgstr ""
"Brug den første rulleboks til at ændre hvor mange spilsessioner, der skal "
"optages. før du kan begynde at se resultaterne i historikgrafikken."

#. (itstool) path: item/p
#: C/history.page:68
msgid ""
"Use the second spin box to change how many game sessions will be shown in "
"the history graphic."
msgstr ""
"Brug den anden rulleboks til at ændre hvor mange spilsessioner, der vil "
"blive vist i historikgrafikken."

#. (itstool) path: info/desc
#: C/multiplayer.page:7
msgid "How to play with other people."
msgstr "Sådan spiller du med andre."

#. (itstool) path: page/title
#: C/multiplayer.page:19
msgid "Play with other people"
msgstr "Spil med andre"

#. (itstool) path: page/p
#: C/multiplayer.page:21
msgid ""
"It is not possible to play against other people over the Internet or a local "
"network with <app>gbrainy</app>."
msgstr ""
"Det er ikke muligt at spille <app>gbrainy</app> mod andre over internettet "
"eller et lokalt netværk."

#. (itstool) path: info/desc
#: C/score.page:8
msgid "How the player scores are calculated."
msgstr "Hvordan spillerpoint beregnes."

#. (itstool) path: page/title
#: C/score.page:19
msgid "Game score and timings"
msgstr "Point og tid"

#. (itstool) path: page/p
#: C/score.page:20
msgid "If you answer a puzzle incorrectly, you will not get any score for it."
msgstr ""
"Hvis du svarer forkert på et puslespil, vil du ikke modtage nogen point."

#. (itstool) path: page/p
#: C/score.page:23
msgid ""
"If you answer a puzzle correctly, you will get a score which depends on the "
"time taken to resolve the problem and whether you used the tip during the "
"game."
msgstr ""
"Hvis du svarer korrekt, vil du få point afhængig af hvor lang tid, du var om "
"at løse problemet, og efter hvorvidt du brugte fif i spillet."

#. (itstool) path: page/p
#: C/score.page:27
msgid ""
"The following table summarizes the different game durations (in seconds) "
"based on the difficulty level."
msgstr ""
"Den følgende tabel opsummerer de forskellige spilvarigheder (i sekunder) "
"baseret på sværhedsgraden."

#. (itstool) path: td/p
#: C/score.page:34
msgid "Easy"
msgstr "Let"

#. (itstool) path: td/p
#: C/score.page:35
msgid "Medium"
msgstr "Mellem"

#. (itstool) path: td/p
#: C/score.page:36
msgid "Master"
msgstr "Øvet"

#. (itstool) path: td/p
#: C/score.page:40
msgid "156"
msgstr "156"

#. (itstool) path: td/p
#: C/score.page:41
msgid "120"
msgstr "120"

#. (itstool) path: td/p
#: C/score.page:42
msgid "110"
msgstr "110"

#. (itstool) path: td/p
#: C/score.page:45
msgid "Mental calculation"
msgstr "Hovedregning"

#. (itstool) path: td/p
#: C/score.page:46
msgid "78"
msgstr "78"

#. (itstool) path: td/p
#: C/score.page:47
msgid "60"
msgstr "60"

#. (itstool) path: td/p
#: C/score.page:48
msgid "55"
msgstr "55"

#. (itstool) path: td/p
#: C/score.page:52 C/score.page:58
msgid "39"
msgstr "39"

#. (itstool) path: td/p
#: C/score.page:53 C/score.page:59
msgid "30"
msgstr "30"

#. (itstool) path: td/p
#: C/score.page:54 C/score.page:60
msgid "27"
msgstr "27"

#. (itstool) path: page/p
#: C/score.page:63
msgid ""
"With the expected time for the chosen difficulty level and the time you take "
"to complete the game, the following logic is applied:"
msgstr ""
"Med det forventede tidsbrug i mente for det aktuelle sværhedsniveau og den "
"reelt forbrugte tid på spillet anvendes den følgende logik:"

#. (itstool) path: item/p
#: C/score.page:69
msgid ""
"If you take less than the time expected to complete the game, you score 10 "
"points."
msgstr ""
"Hvis spilleren brugte mindre end det forventede tidsforbrug er "
"pointtildelingen 10 point."

#. (itstool) path: item/p
#: C/score.page:74
msgid ""
"If you take more than the time expected to complete the game, you score 8 "
"points."
msgstr ""
"Hvis spilleren brugte mere end det forventede tidsforbrug er "
"pointtildelingen 8 point."

#. (itstool) path: item/p
#: C/score.page:79
msgid ""
"If you take more than 2x the time expected to complete the game, you score 7 "
"points."
msgstr ""
"Hvis spilleren brugte mere end 2x det forventede tidsforbrug er "
"pointtildelingen på 7 point."

#. (itstool) path: item/p
#: C/score.page:85
msgid ""
"If you take more than 3x the time expected to complete the game, you score 6 "
"points."
msgstr ""
"Hvis spilleren brugte mere end 3x det forventede tidsforbrug er "
"pointtildelingen på 6 point."

#. (itstool) path: item/p
#: C/score.page:91
msgid "If you use a tip, you score only the 80% of the original score."
msgstr ""
"Hvis du bruger et fif, er pointtildelingen kun på 80 % af den oprindelige "
"pointtildeling."

#. (itstool) path: section/title
#: C/score.page:98
msgid "Computing the totals"
msgstr "Beregning af totalerne"

#. (itstool) path: section/p
#: C/score.page:99
msgid ""
"<app>gbrainy</app> keeps track of the different games types played. To "
"compute the final score of every set of game types it sums all the results "
"of the same game types played and then applies a factor based on: the "
"logarithm of 10 for the easy level; on the logarithm of 20 for the medium "
"level; and on the logarithm of 30 for the master level."
msgstr ""
"<app>gbrainy</app> holder historik på de forskellige spiltyper, der er "
"spillet. For at beregne den samlede sum for alle spiltyper summer spillet "
"resultaterne for de enkelte spiltyper og ganger så en faktor til baseret på "
"en logaritme på 10 for sværhedsgraden let, 20 for mellem og 30 for øvet."

#. (itstool) path: section/p
#: C/score.page:106
msgid ""
"This means that when playing at medium difficulty level, to get a score of "
"100 points you need to score 10 points on at least 20 games of every game "
"type played."
msgstr ""
"Dette betyder, at for at få 100 point på sværhedsgraden mellem skal du have "
"mindst 10 point i mindst 20 spil af hver spiltype."

#. (itstool) path: section/p
#: C/score.page:110
msgid ""
"This may sound challenging, but it allows players to compare game scores "
"from different sessions (in the player's game history) and allows better "
"tracking of the progression of the player through all of the games played."
msgstr ""
"Dette kan lyde indviklet men gør at spillere kan sammenligne point fra "
"forskellige sessioner (i spillerhistorikken) og gør, at det er nemmere at "
"følge udviklingen for en spiller over alle de spil, som denne har spillet."

#. (itstool) path: info/desc
#: C/gameplay.page:7
msgid "Introduction to <app>gbrainy</app>."
msgstr "Introduktion til <app>gbrainy</app>."

#. (itstool) path: page/title
#: C/gameplay.page:20
msgid "Gameplay"
msgstr "Spil"

#. (itstool) path: page/p
#: C/gameplay.page:22
msgid ""
"<app>gbrainy</app> is a brain teaser game; the aim of the game is to have "
"fun and keep your brain trained."
msgstr ""
"<app>gbrainy</app> er et grublespil, der kan holde din hjerne i tip-top "
"stand."

#. (itstool) path: page/p
#: C/gameplay.page:26
msgid ""
"It features different game types like logic puzzles, mental calculation "
"games, memory trainers and verbal analogies, designed to test different "
"cognitive skills."
msgstr ""
"Det har forskellige spiltyper som logiske puslespil, hovedregning, "
"hukommelsestræning og sproglige analogier designet til at teste forskellige "
"kognitative evner."

#. (itstool) path: page/p
#: C/gameplay.page:30
msgid ""
"You can choose different difficulty levels making <app>gbrainy</app> "
"enjoyable for kids, adults or senior citizens. It also features a game "
"history, personal records, tips and fullscreen mode support. <app>gbrainy</"
"app> can also be <link href=\"https://wiki.gnome.org/Apps/gbrainy/Extending"
"\">extended</link> easily with new games developed by third parties."
msgstr ""
"Du kan vælge forskellige sværhedsgrader, hvilket gør, at <app>gbrainy</app> "
"kan nydes af børn, voksne og ældre. Det har også en spilhistorik, spillerens "
"personlige rekorder, tip og understøttelse af fuldskærm. <app>gbrainy</app> "
"kan også nemt <link href=\"https://wiki.gnome.org/Apps/gbrainy/Extending"
"\">udvides</link> med spil udviklet af andre."

#. (itstool) path: page/p
#: C/gameplay.page:37
msgid ""
"<app>gbrainy</app> relies heavily on the work of previous puzzle masters, "
"ranging from classic puzzles from ancient times to more recent works like "
"<link href=\"https://en.wikipedia.org/wiki/Terry_Stickels\">Terry Stickels'</"
"link> puzzles or the classic <link href=\"https://en.wikipedia.org/wiki/Dr."
"_Brain\">Dr. Brain</link> game."
msgstr ""
"<app>gbrainy</app> afhænger i høj grad af tidligere puslemestres talenter, "
"lige fra klassiske historiske spil til nyere ideer som <link href=\"http://"
"en.wikipedia.org/wiki/Terry_Stickels\">Terry Stickels</link> puslespil eller "
"<link href=\"http://en.wikipedia.org/wiki/Dr._Brain\">Dr. Brains</link> "
"klassiske spil."

#. (itstool) path: note/p
#: C/gameplay.page:45
msgid ""
"There have been recent discussions in the scientific community regarding "
"whether brain training software improves cognitive performance. Most of the "
"studies show that there is little or no improvement, but that doesn't mean "
"you can't have a good time playing games like <app>gbrainy</app>!"
msgstr ""
"Der har været diskussioner i det videnskabelige samfund om hvorvidt det at "
"spille denne slags spil kan forbedre ens kognitive præstationsevne. De "
"fleste studier viser ingen eller en meget lille forbedring. Du kan dog "
"stadig have det sjovt ved at spille <app>gbrainy.</app>."

#. (itstool) path: info/title
#: C/index.page:8 C/index.page:12
msgctxt "link"
msgid "gbrainy"
msgstr "gbrainy"

#. (itstool) path: info/title
#: C/index.page:9
msgctxt "text"
msgid "gbrainy"
msgstr "gbrainy"

#. (itstool) path: info/desc
#: C/index.page:13
msgid "The <app>gbrainy</app> help."
msgstr "Hjælp til <app>gbriany</app>."

#. (itstool) path: page/title
#: C/index.page:23
msgid "<_:media-1/> gbrainy"
msgstr "<_:media-1/> gbrainy"

#. (itstool) path: section/title
#: C/index.page:29
msgid "Basic Gameplay &amp; Usage"
msgstr "Grundlæggende spil &amp; brug"

#. (itstool) path: section/title
#: C/index.page:33
msgid "Multiplayer Game"
msgstr "Flerspiller"

#. (itstool) path: section/title
#: C/index.page:37
msgid "Tips &amp; Tricks"
msgstr "Fif &amp; trik"

#. (itstool) path: info/title
#: C/newgame.page:7
msgctxt "sort"
msgid "1"
msgstr "1"

#. (itstool) path: info/desc
#: C/newgame.page:10
msgid "Start and play a game."
msgstr "Begynd at spille."

#. (itstool) path: page/title
#: C/newgame.page:22
msgid "Game session"
msgstr "Spilsession"

#. (itstool) path: section/title
#: C/newgame.page:25
msgid "Start a new session"
msgstr "Begynd en ny session"

#. (itstool) path: section/p
#: C/newgame.page:26
msgid "To start a new game, do one of the following:"
msgstr "For at begynde på et nyt spil, vælges en af følgende muligheder:"

#. (itstool) path: item/p
#: C/newgame.page:31
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">New "
"Game</gui></guiseq>, and select the type of game to play."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Spil</gui><gui style=\"menuitem\">Nyt spil</"
"gui></guiseq>, og vælg spiltype."

#. (itstool) path: item/p
#: C/newgame.page:37
msgid "Click on one of the buttons in the toolbar."
msgstr "Klik på en af knapperne i værktøjsbjælken."

#. (itstool) path: section/p
#: C/newgame.page:42
msgid "The toolbar buttons have the following meanings:"
msgstr "Knapperne på værktøjsbjælken har de følgende betydninger:"

#. (itstool) path: item/title
#: C/newgame.page:50
msgid "All"
msgstr "Alle"

#. (itstool) path: item/p
#: C/newgame.page:51
msgid "Starts a new game playing all the available game types."
msgstr "Starter et nyt spil med alle de tilgængelige spiltyper."

#. (itstool) path: item/title
#: C/newgame.page:69
msgid "Logic"
msgstr "Logik"

#. (itstool) path: item/p
#: C/newgame.page:70
msgid "Start a new game playing only logic games."
msgstr "Starter et nyt spil udelukkende med logiske spil."

#. (itstool) path: item/title
#: C/newgame.page:88
msgid "Calculation"
msgstr "Beregning"

#. (itstool) path: item/p
#: C/newgame.page:89
msgid "Starts a new game playing only calculation games."
msgstr "Starter et nyt spil udelukkende med beregningsspil."

#. (itstool) path: item/title
#: C/newgame.page:107
msgid "Memory"
msgstr "Hukommelse"

#. (itstool) path: item/p
#: C/newgame.page:108
msgid "Starts a new game playing only memory games."
msgstr "Starter et nyt spil udelukkende med hukommelsesspil."

#. (itstool) path: item/title
#: C/newgame.page:126
msgid "Verbal"
msgstr "Sproglig"

#. (itstool) path: item/p
#: C/newgame.page:127
msgid "Starts a new game playing only verbal analogy games."
msgstr "Starter et nyt spil udelukkende med sproglige analogier."

#. (itstool) path: note/p
#: C/newgame.page:143
msgid ""
"These descriptions also apply to the game types that you can select from the "
"<gui style=\"menu\">Game</gui> menu."
msgstr ""
"Disse beskrivelser gælder også for spiltyper, som du kan vælge fra menuen "
"<gui style=\"menu\">Spil</gui>."

#. (itstool) path: section/title
#: C/newgame.page:150
msgid "Play a session"
msgstr "Spil en session"

#. (itstool) path: note/p
#: C/newgame.page:152
msgid "When you play a game, always read the instructions carefully!"
msgstr "Når du spiller så husk altid omhyggeligt at læse instruktionerne!"

#. (itstool) path: section/p
#: C/newgame.page:156
msgid ""
"The game session begins by showing you the problem and then asking for the "
"answer. At the bottom of the window is the main set of controls that you can "
"use to interact with the game."
msgstr ""
"Spilsessionen begynder med at vise dig problemet og så spørge efter svaret. "
"Nederst i vinduet er de væsentligste styringsværktøjer som du kan bruge til "
"at styre spillet med."

#. (itstool) path: section/p
#: C/newgame.page:160
msgid ""
"Once you know the answer to the problem, type it in the <gui style=\"input"
"\">Answer</gui> text entry and press <gui style=\"button\">OK</gui>."
msgstr ""
"Når du kender svaret, skal du indtaste det indtastningsfeltet <gui style="
"\"input\">Svar</gui> og tryk på <gui style=\"button\">O.k.</gui>."

#. (itstool) path: section/p
#: C/newgame.page:164
msgid "To proceed to the next game, click <gui style=\"button\">Next</gui>."
msgstr ""
"For at fortsætte til det næste spil, så klik på <gui style=\"button\">Næste</"
"gui>."

#. (itstool) path: note/p
#: C/newgame.page:168
msgid ""
"If you click <gui style=\"button\">Next</gui> before completing the game, or "
"without providing an answer, that game will be counted in your results."
msgstr ""
"Hvis du klikker på <gui style=\"button\">Næste</gui> før du har færdiggjort "
"spillet, eller uden at angive et svar, vil spillet blive medtaget i dine "
"resultater."

#. (itstool) path: info/desc
#: C/times.page:7
msgid "Change the challenge presentation time in memory games."
msgstr "Ændre den tid som du har til at løse en opgave i hukommelsesspil."

#. (itstool) path: page/title
#: C/times.page:19
msgid "Change the challenge duration"
msgstr "Ændre den tid du har til at løse en opgave"

#. (itstool) path: page/p
#: C/times.page:20
msgid ""
"To change the number of seconds you are given to memorize the challenge when "
"playing memory games, proceed as follows:"
msgstr ""
"For at ændre det antal sekunder du har til at indlære en udfordring, når du "
"spiller hukommelsesspil, skal du gøre det følgende:"

#. (itstool) path: item/p
#: C/times.page:31
msgid ""
"In the <gui>Memory Games</gui> section, use the spin box to increase or "
"decrease the number of seconds that the challenge will be visible for."
msgstr ""
"I afsnittet <gui>Hukommelsesspil</gui>, brug rulleboksen til at øge eller "
"nedsætte antallet af sekunder som udfordringen vil være synlig."

#. (itstool) path: item/p
#: C/times.page:35
msgid "The default value is 6 seconds."
msgstr "Standardværdien er 6 sekunder."

#. (itstool) path: info/desc
#: C/export.page:7
msgid "Export the games for playing off-line."
msgstr "Eksporter spillene så de kan spilles adskilt fra computeren"

#. (itstool) path: page/title
#: C/export.page:20
msgid "Export the games"
msgstr "Eksporter spillene"

#. (itstool) path: page/p
#: C/export.page:21
msgid ""
"It is possible to export and then print the games provided by the app for "
"playing while not at the computer."
msgstr ""
"Det er muligt at eksportere og så udskrive spillene, som tilbydes af "
"programmet, så de kan spilles uden at computeren er tændt."

#. (itstool) path: page/p
#: C/export.page:25
msgid ""
"The games will be saved in a <file>PDF</file> file in the selected folder."
msgstr "Spillene vil blive gemt i en <file>PDF-fil</file> i den valgte mappe."

#. (itstool) path: page/p
#: C/export.page:28
msgid "To export the games:"
msgstr "For at eksportere spillene:"

#. (itstool) path: item/p
#: C/export.page:33
msgid ""
"Choose <guiseq><gui style=\"menu\">Game</gui><gui style=\"menuitem\">Export "
"Games to PDF for Off-Line Playing</gui></guiseq>."
msgstr ""
"Vælg <guiseq><gui style=\"menu\">Spil</gui><gui style=\"menuitem\">Eksporter "
"spil til PDF så du kan spille uden computeren</gui></guiseq>."

#. (itstool) path: item/p
#: C/export.page:38
msgid ""
"Select the type of games, the difficulty level, and how many games to "
"export. When done click <gui style=\"button\">Save</gui>."
msgstr ""
"Vælg typen af spil, sværhedsgraden og hvor mange spil der skal eksporteres. "
"Når du er færdig så klik på <gui style=\"button\">Gem</gui>."

#~ msgid "milo@ubuntu.com"
#~ msgstr "milo@ubuntu.com"

#~ msgid ""
#~ "@@image: 'figures/all-games.png'; md5=b9b2464dfdb280d8de79b921e03a4705"
#~ msgstr ""
#~ "@@image: 'figures/all-games.png'; md5=b9b2464dfdb280d8de79b921e03a4705"

#~ msgid ""
#~ "@@image: 'figures/logic-games.png'; md5=2679be12956397b6337da869bdbee82d"
#~ msgstr ""
#~ "@@image: 'figures/logic-games.png'; md5=2679be12956397b6337da869bdbee82d"

#~ msgid ""
#~ "@@image: 'figures/math-games.png'; md5=208d189bcd6246b40e3dff184a5d5698"
#~ msgstr ""
#~ "@@image: 'figures/math-games.png'; md5=208d189bcd6246b40e3dff184a5d5698"

#~ msgid ""
#~ "@@image: 'figures/memory-games.png'; md5=2747c731e0a757a98a91c8e141748c75"
#~ msgstr ""
#~ "@@image: 'figures/memory-games.png'; md5=2747c731e0a757a98a91c8e141748c75"

#~ msgid ""
#~ "@@image: 'figures/verbal-games.png'; md5=6559e8d03907794b9983c5501df1aed2"
#~ msgstr ""
#~ "@@image: 'figures/verbal-games.png'; md5=6559e8d03907794b9983c5501df1aed2"

#~ msgid "All games button"
#~ msgstr "Alle spilknapper"

#~ msgid "Logic games button"
#~ msgstr "Logik - spilknapper"

#~ msgid "Calculation games button"
#~ msgstr "Hovedregning - spilknap"

#~ msgid "Memory games button"
#~ msgstr "Knapper til hukommelsesspil"

#~ msgid "Verbal games button"
#~ msgstr "Knapper til sproglige spil"

#~ msgid "@@image: 'figures/gbrainy.png'; md5=d3b523ab1c8d00d00560d3f036d681bb"
#~ msgstr ""
#~ "@@image: 'figures/gbrainy.png'; md5=d3b523ab1c8d00d00560d3f036d681bb"

#~ msgid ""
#~ "<media type=\"image\" mime=\"image/png\" src=\"figures/gbrainy.png"
#~ "\">gbrainy logo</media> gbrainy"
#~ msgstr ""
#~ "<media type=\"image\" mime=\"image/png\" src=\"figures/gbrainy.png\">logo "
#~ "til gbrainy</media> gbrainy"

#~ msgid "Logic games logo"
#~ msgstr "Logo for logiske spil"

#~ msgid "Calculation games logo"
#~ msgstr "Logo til hovedregningsspil"

#~ msgid "Memory games logo"
#~ msgstr "Logo til hukommelsesspil"

#~ msgid "Verbal analogies games logo"
#~ msgstr "Logo til sproglige analogier"
