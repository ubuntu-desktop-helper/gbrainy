<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pause" xml:lang="ro">
      
  <info>
    <title type="sort">4</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="newgame"/>
    <desc>Cum să întrerupeți sau să finalizați un joc.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2010, 2017, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Întreruperea/finalizarea unui joc</title>

  <section id="pause-pause">
    <title>Întreruperea și reluarea unui joc</title>
    <p>Pentru a întrerupe un joc astfel încât să îl puteți relua mai târziu, procedați prin una din metodele următoare:</p>
    <list>
      <item>
        <p>Selectați <guiseq><gui style="menu">Joc</gui><gui style="menuitem">Întrerupe jocul</gui></guiseq>.</p>
      </item>
      <item>
        <p>Apăsați pe butonul <gui style="button">Pauză</gui> din bara de unelte.</p>
      </item>
    </list>
    <p>Pentru a relua jocul după ce l-ați întrerupt, procedați prin una din următoarele:</p>
    <list>
      <item>
        <p>Selectați <guiseq><gui style="menu">Joc</gui><gui style="menuitem">Întrerupe jocul</gui></guiseq>.</p>
      </item>
      <item>
        <p>Apăsați pe butonul <gui style="button">Reia</gui> din bara de unelte.</p>
      </item>
    </list>
  </section>
  
  <section id="pause-stop">
    <title>Finalizarea unei partide</title>
    <p>Pentru finaliza un joc, faceți una din următoarele:</p>
    <list>
      <item>
        <p>Selectați <guiseq><gui style="menu">Joc</gui><gui style="menuitem">Termină jocul</gui></guiseq>.</p>
      </item>
      <item>
        <p>Apăsați pe butonul <gui style="button">Finalizează</gui> din bara de unelte.</p>
      </item>
    </list>
  </section>
</page>
