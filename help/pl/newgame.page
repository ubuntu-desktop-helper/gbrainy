<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="newgame" xml:lang="pl">
      
  <info>
    <title type="sort">1</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gametypes"/>
    <desc>Rozpoczynanie gry.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>
   <credit type="author">
    <name>Milo Casagrande</name>
    <email>milo@ubuntu.com</email>
   </credit>
   <license>
     <p>Creative Commons Share Alike 3.0</p>
   </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Sesja</title>
  
  <section id="game-start">
    <title>Rozpoczynanie nowej sesji</title>
    <p>Aby rozpocząć nową grę, wykonaj jedno z tych działań:</p>
    <list>
      <item>
        <p>Wybierz <guiseq><gui style="menu">Gra</gui><gui style="menuitem">Nowa</gui></guiseq> i wybierz rodzaj zadań.</p>
      </item>
      <item>
        <p>Kliknij jeden z przycisków na pasku narzędziowym.</p>
      </item>
    </list>
    <p>Dostępne przyciski na pasku narzędziowym:</p>
    <table frame="none" rules="none" shade="none">
      <tr>
        <td>
        <terms>
          <item>
            <title>Wszystkie</title>
            <p>Rozpoczyna nową grę ze wszystkimi rodzajami zadań.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/all-games.png" its:translate="no">
            <p>
              All games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Logiczne</title>
            <p>Rozpoczyna nową grę tylko z zadaniami logicznymi.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
            <p>
            Logic games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Obliczeniowe</title>
            <p>Rozpoczyna nową grę tylko z zadaniami obliczeniowymi.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
            <p>
              Calculation games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Pamięciowe</title>
            <p>Rozpoczyna nową grę tylko z zadaniami pamięciowymi.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
            <p>
              Memory games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Słowne</title>
            <p>Rozpoczyna nową grę tylko z zadaniami słownymi.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
            <p>
              Verbal games button
            </p>
          </media>
        </td>
      </tr>
    </table>
    <note>
      <p>Te opisy mają zastosowanie tylko do rodzajów zadań, które można wybrać z menu <gui style="menu">Gra</gui>.</p>
    </note>
  </section>
  <section id="game-play">
    <title>Rozgrywanie sesji</title>
    <note style="tip">
      <p>Podczas gry zawsze uważnie czytaj instrukcje!</p>
    </note>
    <p>Sesja zaczyna się wyświetlając problem i prosząc o odpowiedź. Na dole okna znajduje się główny zestaw elementów sterujących.</p>
    <p>Wpisz odpowiedź w polu tekstowym <gui style="input">Odpowiedź</gui> i kliknij przycisk <gui style="button">OK</gui>.</p>
    <p>Aby przejść do następnej gry, kliknij przycisk <gui style="button">Następna</gui>.</p>
    <note>
      <p>Kliknięcie przycisku <gui style="button">Następna</gui> przed ukończeniem zadania lub bez podania odpowiedzi spowoduje dodanie tego zadania do wyników.</p>
    </note>
  </section>
</page>
