<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="gametypes" xml:lang="pl">
  <info>
    <title type="sort">3</title>
    <desc>Jakie rodzaje zadań można rozwiązywać.</desc>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gameplay"/>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>    
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>
  <title>Rodzaje zadań</title>

  <p><app>gbrainy</app> zawiera te rodzaje zadań:</p>
  
  <table frame="none" rules="none" shade="none">
    <tr>
      <td>
      <terms>
        <item>
          <title>Łamigłówki logiczne</title>
          <p>Zadania zaprojektowane do sprawdzania umiejętności dedukcji i myślenia. Są one oparte na sekwencjach elementów, dedukcji wizualnej i przestrzennej oraz związkach między elementami.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
          <p>
            Logic games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Zadania obliczeniowe</title>
          <p>Zadania oparte na działaniach arytmetycznych, zaprojektowane do treningu umiejętności liczenia. Wymagają zastosowania mnożenia, dzielenia, dodawania i odejmowania w różnych połączeniach.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
          <p>
            Calculation games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Zadania pamięciowe</title>
          <p>Zadania zaprojektowane do sprawdzania pamięci krótkotrwałej. Pokazują one zbiory obiektów i pytają gracza o przypomnienie sobie ich, czasami ustanawiając związki między ilustracjami, słowami, cyframi i kolorami.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
          <p>
            Memory games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Zadania słowne</title>
          <p>Zadania sprawdzające zasób słownictwa. Proszą one o identyfikowanie związków przyczynowo-skutkowych, używanie synonimów lub antonimów i słownictwa.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
          <p>
            Verbal analogies games logo
          </p>
        </media>
      </td>
    </tr>
  </table>
</page>
