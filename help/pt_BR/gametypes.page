<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="gametypes" xml:lang="pt-BR">
  <info>
    <title type="sort">3</title>
    <desc>Quais tipos de jogos você pode jogar.</desc>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gameplay"/>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>    
    <license>
      <p>Creative Commons Compartilhada Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flamarion Jorge</mal:name>
      <mal:email>jorge.flamarion@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>hiko@duck.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Tipos de jogos</title>

  <p><app>gbrainy</app> fornece os seguintes tipos de jogos:</p>
  
  <table frame="none" rules="none" shade="none">
    <tr>
      <td>
      <terms>
        <item>
          <title>Quebra-cabeças lógicos</title>
          <p>Jogos projetados para desafiar suas habilidades de pensar e raciocinar. Esses jogos são baseados em sequências de elementos, raciocínio visual e espacial, ou relacionamentos entre elementos.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
          <p>
            Logic games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Cálculos mentais</title>
          <p>Jogos baseados em operações aritméticas projetadas para melhorar suas habilidades de cálculos mentais. Jogos que exigem que o jogador use multiplicação, divisão, adição e subtração combinadas em formas variadas.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
          <p>
            Calculation games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Treino de memória</title>
          <p>Jogos projetados para desafiar sua memória a curto prazo. Esses jogos mostram coleções de objetos e pedem que o jogador lembre-se deles, em alguns casos estabelecendo relação entre imagens, palavras, números ou cores.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
          <p>
            Memory games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Analogias verbais</title>
          <p>Jogos que desafiam sua capacidade verbal. Esses jogos pedem que o jogador identifique causa e efeito, use sinônimos ou antônimos, e use seu vocabulário.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
          <p>
            Verbal analogies games logo
          </p>
        </media>
      </td>
    </tr>
  </table>
</page>
