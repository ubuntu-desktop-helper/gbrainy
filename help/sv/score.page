<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="score" xml:lang="sv">
      
  <info>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="history"/>
    <desc>Hur spelarens poäng beräknas.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-22" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons DelaLika 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Josef Andersson</mal:name>
      <mal:email>josef.andersson@fripost.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Spelpoäng och tidtagning</title>
  <p>Om du besvarar ett pussel inkorrekt kommer du inte få några poäng för det.</p>
  <p>Om du klarar ett pussel kommer du att få en poäng som beror på tiden det tog att lösa problemet och huruvida du använde tipset under spelet.</p>
  <p>Följande tabell summerar hur långa de olika spelen är (i sekunder) baserat på deras svårighetsgrad.</p>
  <table frame="all" rules="all">
    <tr>
      <td><p/></td>
      <td><p>Lätt</p></td>
      <td><p>Medel</p></td>
      <td><p>Mästare</p></td>
    </tr>
    <tr>
      <td><p>Logiska pussel</p></td>
      <td><p>156</p></td>
      <td><p>120</p></td>
      <td><p>110</p></td>
    </tr>
    <tr>
      <td><p>Huvudräkning</p></td>
      <td><p>78</p></td>
      <td><p>60</p></td>
      <td><p>55</p></td>
    </tr>
    <tr>
      <td><p>Minnestränare</p></td>
      <td><p>39</p></td>
      <td><p>30</p></td>
      <td><p>27</p></td>
    </tr>
    <tr>
      <td><p>Verbala analogier</p></td>
      <td><p>39</p></td>
      <td><p>30</p></td>
      <td><p>27</p></td>
    </tr>
  </table>
  <p>Med den förväntade tiden för den valda svårighetsgraden och tiden du tar på dig att slutföra spelet tillämpas följande logik:</p>
  <list>
    <item>
      <p>Om du tar mindre tid än den förväntade på dig att slutföra spelet får du 10 poäng.</p>
    </item>
    <item>
      <p>Om du tar längre tid än den förväntade på dig att slutföra spelet får du 8 poäng.</p>
    </item>
    <item>
      <p>Om du tar mer än 2x den förväntade tiden på dig att slutföra spelet får du 7 poäng.</p>
    </item>
    <item>
      <p>Om du tar mer än 3x den förväntade tiden på dig att slutföra spelet får du 6 poäng.</p>
    </item>
    <item>
      <p>Om du använder ett tips får du endast 80% av originalpoängen.</p>
    </item>
  </list>
  
  <section id="computing-scores">
    <title>Beräkna totalen</title>
    <p><app>gbrainy</app> håller reda på de olika typerna av spel som spelas. För att beräkna slutpoängen för varje set av speltyp summeras alla resultat för samma spelade speltyper, och sedan tillämpas en faktor baserad på: logaritmen 10 för lätta nivån; logaritmen 20 för mellannivån; och logaritmen 30 för mästarnivån.</p>
	  <p>Det betyder att när du spelar ett spel på svårighetsgraden medel behöver du minst 10 poäng på minst 20 spel för varje typ av spel som spelas för att få 100 poäng.</p>
	  <p>Det kan låta utmanande, men det gör det möjligt för spelare att jämföra spelpoäng från olika sessioner ( i spelarens spelhistorik) och gör det också möjligt att bättre följa spelarens utveckling genom alla spelade spel.</p>
  </section>
</page>
